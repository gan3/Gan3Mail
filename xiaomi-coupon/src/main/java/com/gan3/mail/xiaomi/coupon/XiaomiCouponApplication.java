package com.gan3.mail.xiaomi.coupon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class XiaomiCouponApplication {

    public static void main(String[] args) {
        SpringApplication.run(XiaomiCouponApplication.class, args);
    }

}
