package com.gan3.mail.xiaomi.product;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class XiaomiProductApplication {

    public static void main(String[] args) {
        SpringApplication.run(XiaomiProductApplication.class, args);
    }

}
