package com.gan3.mail.xiaomi.ware;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class XiaomiWareApplication {

    public static void main(String[] args) {
        SpringApplication.run(XiaomiWareApplication.class, args);
    }

}
