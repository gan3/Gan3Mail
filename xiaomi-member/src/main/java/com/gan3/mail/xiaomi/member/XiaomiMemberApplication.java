package com.gan3.mail.xiaomi.member;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class XiaomiMemberApplication {

    public static void main(String[] args) {
        SpringApplication.run(XiaomiMemberApplication.class, args);
    }

}
