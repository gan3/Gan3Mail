package com.gan3.mail.xiaomi.order;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class XiaomiOrderApplication {

    public static void main(String[] args) {
        SpringApplication.run(XiaomiOrderApplication.class, args);
    }

}
